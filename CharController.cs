using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;

[RequireComponent(typeof(CharacterController))]

public class CharController : MonoBehaviour
{
    CharacterController cc;
    Vector3 moveDir;
    public float moveSpeed = 3f;
    Transform cam;
    float axisX;
    float axisY;

    public bool isMoving = false;

    Vector3 lastPos;
    public float movedLastFrame = 0f;

    void Start()
    {
        lastPos = transform.position;
        cc = GetComponent<CharacterController>();
        cam = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        wasd();
        move();
        camControl();
        CalcMovedLastFrame();
    }

    void wasd()
    {
        float fwd = Input.GetKey(KeyCode.W) ? 1 : Input.GetKey(KeyCode.S) ? -1 : 0;
        float side = Input.GetKey(KeyCode.D) ? 1 : Input.GetKey(KeyCode.A) ? -1 : 0;
        isMoving = fwd != 0 || side != 0;
        Vector3 fwdSideBuffer = transform.forward * fwd + transform.right * side;
        fwdSideBuffer = fwdSideBuffer.normalized * Time.deltaTime * moveSpeed;
        fwdSideBuffer += Vector3.down;
        moveDir = fwdSideBuffer;
    }

    void CalcMovedLastFrame()
    {
        movedLastFrame = Vector3.Distance(transform.position, lastPos);
        lastPos = transform.position;
    }

    void camControl()
    {
        axisX = Input.GetAxis("Mouse X");
        axisY = Input.GetAxis("Mouse Y");

        cam.position = transform.position + Vector3.up * 0.8f;
        cam.transform.parent = transform;

        if (axisY != 0) cam.Rotate(Vector3.right * -axisY);
        if (axisX != 0) transform.Rotate(Vector3.up * axisX);
    }

    void move()
    {
        cc.Move(moveDir);
    }
}
