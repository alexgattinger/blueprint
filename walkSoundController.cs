using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class walkSoundController : MonoBehaviour
{
	System.Random rnd;
	public float stepDistance = 1.4f;
	float stepDistanceAggregate = 0;
	Player player;
	
	void Start()
	{
		player = statix.i.player;
		rnd = new System.Random();
	}

	void Update()
	{
		stepDistanceAggregate += player.cc.movedLastFrame;
		if (stepDistanceAggregate >= stepDistance)
		{
			AudioManager.i.PlayOnce("walk0", "walk1", "walk2", "walk3", "walk4");
			statix.i.noiseEvent.Invoke(0.2f, transform.position, "walk");
			stepDistanceAggregate = 0;
		}
	}
}
