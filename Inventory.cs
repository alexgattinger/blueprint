using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item> items = new List<Item>();

    public void AddItem(string itemName, int quantity)
    {
        foreach (var item in items)
        {
            if (item.itemName == itemName)
            {
                item.quantity += quantity;
                return;
            }
        }

        items.Add(new Item(itemName, quantity));
    }

    public bool RemoveItem(string itemName, int quantity)
    {
        foreach (var item in items)
        {
            if (item.itemName == itemName)
            {
                if (item.quantity >= quantity)
                {
                    item.quantity -= quantity;

                    if (item.quantity <= 0)
                    {
                        items.Remove(item);
                    }
                    return true;
                }

                return false;
            }
        }

        return false;
    }

    public bool HasItem(string itemName)
    {
        foreach (var item in items)
        {
            if (item.itemName == itemName && item.quantity > 0)
            {
                return true;
            }
        }

        return false;
    }

    void Start()
    {
        AddItem("Potion", 1);
        AddItem("Sword", 1);
        bool hasPotion = HasItem("Potion");
        Debug.Log("Has Potion: " + hasPotion);

        RemoveItem("Potion", 1);
        hasPotion = HasItem("Potion");
        Debug.Log("Has Potion: " + hasPotion);
    }
}