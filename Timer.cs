using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
	public static Timer i;

	List<TimerObject> timerss = new List<TimerObject>();
	Dictionary<string, float> timers = new Dictionary<string, float>();
	Dictionary<string, UnityEvent> events = new Dictionary<string, UnityEvent>();
	// Start is called before the first frame update
	void Start()
	{
		i = this;
	}

	public void StartTimer(string name, float time)
	{
		timerss.Add(new TimerObject(name, time));
	}

	public void ResetTimer(string name)
	{
		Debug.Log("Timer reset");
		TimerObject to = timerss.Find(x => x.name == name);
		if (to != null)
		{
			to.time = to.originalTime;
		}
	}

	public UnityEvent StartTimerEvent(string name, float time)
	{
		TimerObject to = timerss.Find(x => x.name == name);
		if (to != null)
		{
			to.time = time;
			return to.e;
		}
		UnityEvent ne = new UnityEvent();
		timerss.Add(new TimerObject(name, time, ne));
		return ne;
	}

	public void DisposeTimer(string name)
	{
		timerss.RemoveAll(x => x.name == name);
	}

	public float GetTime(string name)
	{
		return timers[name];
	}

	public void Dispose(string name)
	{
		TimerObject obj = timerss.Find(x => x.name == name);

		if (obj != null && obj.beingDisposed == false)
		{
			obj.e.RemoveAllListeners();
			timerss.RemoveAll(x => x.name == name);
		}
	}

	// Update is called once per frame
	void Update()
	{
		List<string> keysToRemove = new List<string>();

		for (int i = 0; i < timerss.Count; i++)
		{
			if (timerss[i].paused) continue;
			float newTime = timerss[i].time - Time.deltaTime;
			timerss[i].time = newTime;
			if (newTime <= 0)
			{
				if (timerss[i].e != null)
				{
					timerss[i].beingDisposed = true;
					timerss[i].e.Invoke();
					timerss[i].e.RemoveAllListeners();
				}

				keysToRemove.Add(timerss[i].name);
			}
		}

		foreach (string key in keysToRemove)
		{
			timerss.RemoveAll(x => x.name == key);
		}
	}

	string[] getTimersNames(List<TimerObject> timers)
	{
		string[] names = new string[timers.Count];
		int i = 0;
		foreach (TimerObject timer in timers)
		{
			names[i] = timer.name;
			i++;
		}

		return names;
	}

}
