using System.Collections;
using System.Collections.Generic;
using Mono.CSharp;
using UnityEngine;

public class DoorController : Interactable
{
    bool doorOpen = false;
    float originalY;
    public float modifier = 1;

    private void Start()
    {
        this.init();
        originalY = transform.eulerAngles.y;
    }

    public override void interact()
    {
        if (!canInteract)
        {
            return;
        }
        canInteract = false;
        if (!doorOpen)
        {
            AudioManager.i.PlayOnce("fatdooropen");
            Tweener.Tween(uid + "doorrot1", 0, 5, (from, to) =>
            {
                transform.rotation = Quaternion.Euler(0, originalY + (doorOpen ? 1 : -1) * modifier * from * 25, 0);
            });
        }
        if (doorOpen)
        {
            AudioManager.i.PlayOnce("fatdoorclose");
            Tweener.Tween(uid + "doorrot2", 0, 5, (from, to) =>
            {
                transform.rotation = Quaternion.Euler(0, (originalY + 125 * modifier) + (doorOpen ? 1 : -1) * modifier * from * 25, 0);
            });
        }
        Timer.i.StartTimerEvent("doorOpen" + gameObject.GetInstanceID(), 5f).AddListener(() =>
        {
            canInteract = true;
        });
        doorOpen = !doorOpen;
    }
}
