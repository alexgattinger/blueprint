using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Door : Interactable
{
    public UnityEvent doorTrigger;
    StateMachine sm;
    State doorStateOpen;
    State doorStateClosed;

    //START
    void Start()
    {
        sm = new StateMachine();
        doorStateOpen = new State();
        doorStateClosed = new State();
        sm.ChangeState(doorStateClosed);
        triggerInteractable();
    }

    public override void triggerInteractable()
    {
        Debug.Log("trigger from child");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
