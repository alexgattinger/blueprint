using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Scanner : MonoBehaviour
{
    RaycastHit hit;
    public float scanRange = 2f;
    public bool anyObjectInScanner = false;
    public bool interactableObjectInScanner = false;
    bool raycastHasHit = false;
    GameObject hitObject;
    public float rangeToObject;
    public float rangeToObjectPivot;
    public Interactable interactable;

    private void Update() {
        scan();
    }

    public void scan()
    {
        if (Physics.Raycast(transform.position, transform.forward, out hit, scanRange))
        {
            raycastHasHit = true;
            if (hit.collider.gameObject.GetComponent("Interactable") != null)
            {
                interactableObjectInScanner = true;
                interactable = (Interactable)hit.collider.gameObject.GetComponent("Interactable");
            }
            else
            {
                interactableObjectInScanner = false;
                interactable = null;
            }
        }
        else
        {
            raycastHasHit = interactableObjectInScanner = false;
            interactable = null;
        }
        anyObjectInScanner = raycastHasHit;
    }
}
