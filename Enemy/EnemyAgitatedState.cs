using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAgitatedState : State
{
    Enemy enemy;
    Player player;

    bool canMakeSound = true;
    public EnemyAgitatedState(Enemy enemy, Player player)
    {
        this.name = "agitated";
        this.enemy = enemy;
        this.player = player;
    }

    public override void Enter()
    {
        statix.i.noiseEvent.AddListener(hearedNoise);
        AudioManager.i.PlayOnce("growl_low");
        enemy.stateText.text = "Agitated";
        enemy.ConfAgent(isStopped: false, speed: 2.56f, destination: statix.i.player.transform.position);
        Timer.i.StartTimerEvent("agitatedsearch", 10f).AddListener(enemy.startWalking);
    }

    void hearedNoise(float noiseAmount, Vector3 noisePosition, string noiseType)
    {
        if(canMakeSound){
            Timer.i.StartTimerEvent("canhearsound", 6f).AddListener(() => {
                canMakeSound = true;
                return;
            });
            canMakeSound = false;
            AudioManager.i.PlayOnce("growl_low");
        }
        if (enemy.WeightedNoiseLevel(noiseAmount, noisePosition) >= 0.1f)
        {
            enemy.monsterHearsSoundIndicator.SetActive(true);
            Timer.i.StartTimerEvent("removeIndicator", 0.1f).AddListener(() => {
                enemy.monsterHearsSoundIndicator.SetActive(false);
            });
            enemy.na.SetDestination(statix.i.player.transform.position);
            Timer.i.ResetTimer("agitatedsearch");
        }
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        statix.i.noiseEvent.RemoveListener(hearedNoise);
        Timer.i.Dispose("agitatedsearch");
    }
}
