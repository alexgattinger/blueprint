using System.Collections;
using System.Collections.Generic;
using Mono.CSharp;
using UnityEngine;

public class EnemyAttackState : State
{
    Enemy enemy;
    Player player;

    public EnemyAttackState(Enemy enemy, Player player)
    {
        this.name = "attack";
        this.enemy = enemy;
        this.player = player;

    }

    public override void Enter()
    {
        Timer.i.StartTimerEvent("attacking", 3f).AddListener(resumeHunt);
        AudioManager.i.PlayOnce("enemy_hit");
        enemy.stateText.text = "attacking";
        enemy.na.isStopped = true;
        enemy.na.speed = 4f;
    }

    void resumeHunt() {
        Debug.Log("resume hunt");
        enemy.sm.ChangeState(enemy.enemyHuntState);
    }

    public override void Execute()
    {

    }

    public override void Exit()
    {
    }
}
