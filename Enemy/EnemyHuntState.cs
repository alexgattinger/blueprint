using System.Collections;
using System.Collections.Generic;
using Mono.CSharp;
using UnityEngine;

public class EnemyHuntState : State
{
    Enemy enemy;
    Player player;

    public EnemyHuntState(Enemy enemy, Player player)
    {
        this.name = "hunt";
        this.enemy = enemy;
        this.player = player;
    }

    public override void Enter()
    {
        statix.i.noiseEvent.AddListener(hearedNoise);
        Timer.i.StartTimerEvent("hunting", 20f).AddListener(enemy.startWalking);
        enemy.stateText.text = "hunting";
        enemy.na.isStopped = false;
        enemy.na.speed = 4f;
        AudioManager.i.PlayOnce("growl_hunt");
    }

    public override void Execute()
    {
       
    }

    void hearedNoise(float noiseAmount, Vector3 noisePosition, string noiseType)
    {
        if (enemy.WeightedNoiseLevel(noiseAmount, noisePosition) >= 0.1f)
        {
            enemy.lookAtPlayer();
            enemy.monsterHearsSoundIndicator.SetActive(true);
            Timer.i.StartTimerEvent("removeIndicator", 0.1f).AddListener(() =>
            {
                enemy.monsterHearsSoundIndicator.SetActive(false);
            });
            enemy.na.SetDestination(statix.i.player.transform.position);
            Timer.i.ResetTimer("hunting");
        }
    }

    public override void Exit()
    {
        Timer.i.Dispose("hunting");
        statix.i.noiseEvent.RemoveListener(hearedNoise);
    }
}
