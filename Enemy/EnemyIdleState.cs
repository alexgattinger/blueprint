using System.Collections;
using System.Collections.Generic;
using Mono.CSharp;
using UnityEngine;

public class EnemyIdleState : State
{
    Enemy enemy;
    Player player;

    

    public EnemyIdleState(Enemy enemy, Player player)
    {
        this.name = "idle";
        this.enemy = enemy;
        this.player = player;
    }

    public override void Enter()
    {
        enemy.stateText.text = "Idle";
        enemy.na.isStopped = true;
        Timer.i.StartTimerEvent("findNewPoi", 5f).AddListener(enemy.startWalking);
    }

    public override void Execute()
    {
        
    }

    public override void Exit()
    {
        Timer.i.Dispose("findNewPoi");
    }
}
