using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder;

public class EnemyWalkState : State
{
    Enemy enemy;
    Player player;

    public EnemyWalkState(Enemy enemy, Player player)
    {
        this.name = "walk";
        this.enemy = enemy;
        this.player = player;
    }

    public override void Enter()
    {
        enemy.stateText.text = "Walking";
        enemy.na.isStopped = false;
        enemy.startPatrol();
        enemy.na.speed = 1.4f;
    }

    public override void Execute()
    {
        if (Vector3.Distance(enemy.currentPoi.position, enemy.transform.position) <= 1f)
        {
            enemy.sm.ChangeState(enemy.idleState);
        }
    }

    public override void Exit()
    {
    }
}
