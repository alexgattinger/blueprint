using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : MonoBehaviour
{
	public StateMachine sm = new StateMachine();
	public NavMeshAgent na;
	public EnemyIdleState idleState;
	EnemyWalkState walkState;
	public EnemyAgitatedState agitatedState;
	public EnemyHuntState enemyHuntState;
	public EnemyAttackState attackState;
	public Transform currentPoi;
	public float noiseLevel = 0f;
	public bool depleteNoiseLevel = true;
	public Slider noiseLevelSlider;
	public float hearingRange = 30f;
	public GameObject monsterHearsSoundIndicator;
	public Text stateText;

	public Transform neck;

	public float distanceToPlayer = 10f;
	Player player;

	void Start()
	{
		player = statix.i.player;
		idleState = new EnemyIdleState(this, statix.i.player);
		walkState = new EnemyWalkState(this, statix.i.player);
		agitatedState = new EnemyAgitatedState(this, statix.i.player);
		enemyHuntState = new EnemyHuntState(this, statix.i.player);
		attackState = new EnemyAttackState(this, statix.i.player);
		na = GetComponent<NavMeshAgent>();
		sm.ChangeState(idleState);
		statix.i.noiseEvent.AddListener(soundEvent);
	}

	public void lookAtPlayer()
	{
		neck.LookAt(player.transform.position);
		neck.Rotate(0, 180f, 0);
	}

	void checkForStateChangeConditions()
	{
		if (noiseLevel >= 0.38f && (sm.currentState.name == "walk" || sm.currentState.name == "idle"))
		{
			sm.ChangeState(agitatedState);
		}
		if (noiseLevel >= 0.81 && (sm.currentState.name == "walk" || sm.currentState.name == "idle" || sm.currentState.name == "agitated"))
		{
			sm.ChangeState(enemyHuntState);
		}
		if (sm.currentState.name == "hunt" && distanceToPlayer <= 1.5f)
		{
			Debug.Log("ATTACK");
			sm.ChangeState(attackState);
		}
	}

	void CalcDistanceToPlayer()
	{
		distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
	}

	public void ConfAgent(bool? isStopped = null, float? speed = null, Vector3? destination = null)
	{
		if (isStopped.HasValue)
			na.isStopped = isStopped.Value;

		if (speed.HasValue)
			na.speed = speed.Value;

		if (destination.HasValue)
			na.SetDestination(destination.Value);
	}

	void soundEvent(float volume, Vector3 pos, string soundName = "")
	{
		noiseLevel += WeightedNoiseLevel(volume, pos);
	}
	public float WeightedNoiseLevel(float volume, Vector3 pos)
	{
		return volume * Mathf.Clamp(1 - Vector3.Distance(transform.position, pos) / hearingRange, 0f, 1f);
	}
	void CalcNoiseLevel()
	{
		if (depleteNoiseLevel)
		{
			noiseLevel -= (Time.deltaTime * 0.1f);
		}
		noiseLevel = Mathf.Clamp(noiseLevel, 0, 1);
		noiseLevelSlider.value = noiseLevel;
	}

	public void startPatrol()
	{
		currentPoi = statix.i.pois[Randomizer.getRandomInt(0, statix.i.pois.Count)];
		na.SetDestination(currentPoi.position);
	}

	public void startWalking()
	{
		sm.ChangeState(walkState);
	}

	void Update()
	{
		CalcNoiseLevel();
		sm.Run();
		checkForStateChangeConditions();
		CalcDistanceToPlayer();
	}
}
