using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimerObject
{
    public string name;
    public float time;
    public float originalTime;
    public UnityEvent e;
    public bool paused = false;
    public bool beingDisposed = false;

    public TimerObject(string _name, float _time, UnityEvent _e = null, bool _paused = false)
    {
        name = _name;
        time = _time;
        e = _e;
        paused = _paused;
        originalTime = _time;
    }
}
