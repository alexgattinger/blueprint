using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Tweener : MonoBehaviour
{
    public static Tweener i;

    void Start()
    {
        i = this;
    }

    public static List<TweenerObject> tweens = new List<TweenerObject>();

    public static float Tween(string _name, float from, float to, Action<float, float> action = null)
    {
        for (int i = 0; i < tweens.Count; i++)
        {
            TweenerObject tween = tweens[i];
            if (tween.name == _name)
            {
                return tween.from;
            }
        }
        tweens.Add(new TweenerObject(_name, from, to, action));
        return from;
    }

    private void Update()
    {
        for (int i = 0; i < tweens.Count; i++)
        {

            TweenerObject tween = tweens[i];
            tween.from = Mathf.Clamp(tween.from + Time.deltaTime * tween.direction, tween.from, tween.to);

            if (tween.action != null) tween.action.Invoke(tween.from, tween.to);
            if (tween.from == tween.to)
            {
                tweens.Remove(tween);
            }
        }
    }

}

