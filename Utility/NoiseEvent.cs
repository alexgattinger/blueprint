using System.Numerics;
using UnityEngine.Events;

[System.Serializable]
public class NoiseEvent : UnityEvent<float, UnityEngine.Vector3, string>
{
    
}
