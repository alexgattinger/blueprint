using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class TweenerObject
{
    public string name;
    public float from;
    public float to;
    public int direction = 1;
    public Action<float, float> action = null;

    public TweenerObject(string name, float from, float to, Action<float, float> action = null)
    {
        this.name = name;
        this.from = from;
        this.to = to;
        this.action = action;
        direction = from > to ? -1 : 1;
    }
}
