using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : Entity
{
    public bool canInteract = true; 
    public string description = "";

    public virtual void triggerInteractable()
    {
        Debug.Log("trigger from parent");
    }

    public virtual void interact()
    {
        Debug.Log("interacting");
    }
}


