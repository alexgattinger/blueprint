using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour
{
    public Transform orbitingObject; // The object that will orbit
    public Transform centerObject;   // The object to orbit around

    public float orbitSpeed = 10.0f; // Speed of the orbit
    public float orbitDistance = 5.0f; // Distance of the orbiting object from the center

    private Vector3 orbitAxis = Vector3.up; // Orbit around the up axis
    private float orbitAngle; // Current angle of the orbit

    void Update()
    {
        if (orbitingObject == null || centerObject == null)
        {
            Debug.LogError("Orbiting object and/or center object not assigned.");
            return;
        }

        // Increment the angle of orbit
        orbitAngle += orbitSpeed * Time.deltaTime;
        orbitAngle %= 360f; // Ensure the angle never exceeds 360 degrees

        // Calculate the new position
        Vector3 offset = new Vector3(Mathf.Sin(orbitAngle * Mathf.Deg2Rad), 0, Mathf.Cos(orbitAngle * Mathf.Deg2Rad)) * orbitDistance;
        orbitingObject.position = centerObject.position + offset;

        // Optional: Make the orbiting object face the center
        orbitingObject.LookAt(centerObject);
    }
}