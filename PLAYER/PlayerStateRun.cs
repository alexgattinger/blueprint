using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerStateRun : State
{
    public Player player;
    int soundIndex = 0;
    float moved = 0;
    public PlayerStateRun(Player _player)
    {
        player = _player;
    }
    public override void Enter()
    {
        moved = 1.5f;
        player.cc.enabled = true;
        player.cc.moveSpeed = 5f;
    }

    public override void Execute()
    {
        moved += player.cc.movedLastFrame;
        if (Input.GetKeyUp(KeyCode.LeftShift))
            player.sm.ChangeState(player.playerWalkState);

        if (player.cc.isMoving)
            player.sprintMeter += Time.deltaTime / 5f;

        playRunSound();
    }

    void playRunSound()
    {
        if (moved >= 1.6f)
        {
            moved = 0;
            AudioManager.i.PlayOnceAtPosition("playerrunning" + soundIndex.ToString(), player.transform.position);
            soundIndex = (soundIndex + 1) % 7;
            statix.i.noiseEvent.Invoke(0.4f, player.transform.position, "run");
        }
        return;
    }

    public override void Exit()
    {
    }
}
