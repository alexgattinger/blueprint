using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heartbeat : MonoBehaviour
{

    Player player;
    string lastTimerName = "";
    // Start is called before the first frame update
    void Start()
    {
        player = statix.i.player;
        Debug.Log("start");
    }

    private void OnEnable()
    {
        Debug.Log("OnEnable");
        lastTimerName = "heartbeat" + Time.deltaTime.ToString();
        Timer.i.StartTimerEvent(lastTimerName, 1.5f).AddListener(playHeartbeatSound);
        return;
    }

    private void OnDisable()
    {
        Timer.i.Dispose(lastTimerName);
    }

    void playHeartbeatSound()
    {
        lastTimerName = "heartbeat" + Time.deltaTime.ToString();
        AudioManager.i.PlayOnceAtPosition("heartbeat", transform.position, player.sprintMeter, (1 + player.sprintMeter / 2));
        Timer.i.StartTimerEvent(lastTimerName, 1.5f - (player.sprintMeter)).AddListener(playHeartbeatSound);
        return;
    }
}
