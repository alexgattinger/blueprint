using System.Collections;
using System.Collections.Generic;
using QFSW.QC;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
	public StateMachine sm;
	public CharController cc;
	public State playerDeadState;
	public State playerWalkState;
	public State playerRunState;
	public State playerSneakState;
	public walkSoundController ws;
	public Scanner scanner;
	public Heartbeat heartbeat;
	public float sprintMeter = 0;
	void Start()
	{
		heartbeat = GetComponent<Heartbeat>();
		playerDeadState = new PlayerStateDead(this);
		playerWalkState = new PlayerStateWalk(this);
		playerRunState = new PlayerStateRun(this);
		playerSneakState = new PlayerStateSneak(this);
		sm = new StateMachine();
		sm.ChangeState(playerWalkState);
	}

	void Update()
	{
		sm.Run();
		interact();
		sprintMeter = Mathf.Clamp(sprintMeter - Time.deltaTime / 10f, 0, 1);
		heartbeat.enabled = sprintMeter > 0;
	}

	void interact()
	{
		scanner.scan();
		if (scanner.interactableObjectInScanner)
		{
			statix.i.interactionFlavorText.gameObject.SetActive(true);
			statix.i.interactionFlavorText.text = "[E] " + scanner.interactable.description;
			if (Input.GetKeyDown(KeyCode.E))
			{
				scanner.interactable.interact();
			}
		} else {
			statix.i.interactionFlavorText.gameObject.SetActive(false);
		}
	}
}
