using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateSneak : State
{
    Player player;
    public PlayerStateSneak(Player _player){
        player = _player;
    }

    public override void Enter()
    {
        player.cc.moveSpeed = 1;
		player.transform.localScale = new Vector3(1, 0.5f, 1);
    }

    public override void Execute()
    {
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            player.sm.ChangeState(player.playerWalkState);
        }
    }

    public override void Exit()
    {
		player.transform.localScale = new Vector3(1, 1, 1);
    }
}
